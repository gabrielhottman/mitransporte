package com.example.mitransporte;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class Configuraciones extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuraciones);
        Log.e("ciclo de vida", "onCreate");
        MostrarMensaje("onCreate");

    }

    private void MostrarMensaje(String mensaje){

        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Ciclo de Vida", "onStart");
        MostrarMensaje("onStart");
   }
}
