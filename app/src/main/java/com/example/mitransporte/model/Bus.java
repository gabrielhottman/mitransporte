package com.example.mitransporte.model;

public class Bus{

    private int fotoBus;
    private String lineaBus;
    private String choferBus;
    private String placaBus;
    private String anioDeUso;
    private String cooperativa;


    public Bus() {

    }

    public Bus(int fotoBus, String lineaBus, String choferBus, String placaBus, String anioDeUso, String cooperativa) {

        this.fotoBus = fotoBus;
        this.lineaBus = lineaBus;
        this.choferBus = choferBus;
        this.placaBus = placaBus;
        this.anioDeUso = anioDeUso;
        this.cooperativa = cooperativa;
    }


    public int getFotoBus() {
        return fotoBus;
    }

    public void setFotoBus(int fotoBus) {
        this.fotoBus = fotoBus;
    }

    public String getLineaBus() {
        return lineaBus;
    }

    public void setLineaBus(String lineaBus) {
        this.lineaBus = lineaBus;
    }

    public String getChoferBus() {
        return choferBus;
    }

    public void setChoferBus(String choferBus) {
        this.choferBus = choferBus;
    }

    public String getPlacaBus() {
        return placaBus;
    }

    public void setPlacaBus(String placaBus) {
        this.placaBus = placaBus;
    }

    public String getAnioDeUso() {
        return anioDeUso;
    }

    public void setAnioDeUso(String anioDeUso) {
        this.anioDeUso = anioDeUso;
    }

    public String getCooperativa() {
        return cooperativa;
    }

    public void setCooperativa(String cooperativa) {
        this.cooperativa = cooperativa;
    }


    @Override
    public String toString() {
        return "(toString)Linea: "+lineaBus;
    }


}
