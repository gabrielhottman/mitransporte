package com.example.mitransporte.model;

import java.io.Serializable;

public class Usuario implements Serializable {

    private String usuario;
    private String correo;
    private String telefono;
    private String lineaBus;


    private String nombres;
    private String apellidos;
    private String cedula;
    private String fechaNacimiento;
    private String genero;
    private String sectorDomicilio;
    private String direccion;


    public Usuario() {

    }

    //Constructor Para registrar usuario por primera vez con los siguientes campos:
    public Usuario(String usuario, String correo, String telefono, String lineaBus) {
        this.usuario = usuario;
        this.correo = correo;
        this.telefono = telefono;
        this.lineaBus = lineaBus;
    }

    //Constructror que sirve para llenar mas campos de la información personal una ves esté logueado
    //NOTA: Todos los campos pueden modificarse, menos el nombre de Usuario


    public Usuario(String nombres, String apellidos, String cedula, String fechaNacimiento,
                   String genero, String sectorDomicilio, String direccion) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.fechaNacimiento = fechaNacimiento;
        this.genero = genero;
        this.sectorDomicilio = sectorDomicilio;
        this.direccion = direccion;
    }

    //Constructor General con todos los campos
    public Usuario(String correo, String telefono, String lineaBus, String nombres, String apellidos,
                   String cedula, String fechaNacimiento, String genero, String sectorDomicilio, String direccion) {
        this.correo = correo;
        this.telefono = telefono;
        this.lineaBus = lineaBus;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.fechaNacimiento = fechaNacimiento;
        this.genero = genero;
        this.sectorDomicilio = sectorDomicilio;
        this.direccion = direccion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLineaBus() {
        return lineaBus;
    }

    public void setLineaBus(String lineaBus) {
        this.lineaBus = lineaBus;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {

        this.genero = genero;
    }

    public String getSectorDomicilio() {
        return sectorDomicilio;
    }

    public void setSectorDomicilio(String sectorDomicilio) {
        this.sectorDomicilio = sectorDomicilio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
