package com.example.mitransporte.model;

public class Recorrido {
    private String nombreRecorrido;
    private String rutaRecorrido;
    private String tiempoQuePasaBus;
    private String lineaBus;

    public Recorrido(String nombreRecorrido, String rutaRecorrido, String tiempoQuePasaBus, String lineaBus) {
        this.nombreRecorrido = nombreRecorrido;
        this.rutaRecorrido = rutaRecorrido;
        this.tiempoQuePasaBus = tiempoQuePasaBus;
        this.lineaBus = lineaBus;
    }

    public String getNombreRecorrido() {
        return nombreRecorrido;
    }

    public void setNombreRecorrido(String nombreRecorrido) {
        this.nombreRecorrido = nombreRecorrido;
    }

    public String getRutaRecorrido() {
        return rutaRecorrido;
    }

    public void setRutaRecorrido(String rutaRecorrido) {
        this.rutaRecorrido = rutaRecorrido;
    }

    public String getTiempoQuePasaBus() {
        return tiempoQuePasaBus;
    }

    public void setTiempoQuePasaBus(String tiempoQuePasaBus) {
        this.tiempoQuePasaBus = tiempoQuePasaBus;
    }

    public String getLineaBus() {
        return lineaBus;
    }

    public void setLineaBus(String lineaBus) {
        this.lineaBus = lineaBus;
    }
}
