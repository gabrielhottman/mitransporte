package com.example.mitransporte.model;

public class MapaPOO {

    private double latitude;
    private double longitude;


    public MapaPOO(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public MapaPOO() {

    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String toString() {

        return "Lactitud: "+latitude+"Longitud: "+longitude;
    }


}
