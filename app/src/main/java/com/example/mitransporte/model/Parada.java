package com.example.mitransporte.model;

import java.util.ArrayList;

public class Parada {
    private String nombreParada;
    private String sectorParada;
    private double latitudParada;
    private double longitudParada;
    private ArrayList<String> lineasQuePasan;


    public Parada() {
    }

    public Parada(String nombreParada, String sectorParada, double latitudParada, double longitudParada, ArrayList<String> lineasQuePasan) {
        this.nombreParada = nombreParada;
        this.sectorParada = sectorParada;
        this.latitudParada = latitudParada;
        this.longitudParada = longitudParada;
        this.lineasQuePasan = lineasQuePasan;
    }

    public String getNombreParada() {
        return nombreParada;
    }

    public void setNombreParada(String nombreParada) {
        this.nombreParada = nombreParada;
    }

    public String getSectorParada() {
        return sectorParada;
    }

    public void setSectorParada(String sectorParada) {
        this.sectorParada = sectorParada;
    }

    public double getLatitudParada() {
        return latitudParada;
    }

    public void setLatitudParada(double latitudParada) {
        this.latitudParada = latitudParada;
    }

    public double getLongitudParada() {
        return longitudParada;
    }

    public void setLongitudParada(double longitudParada) {
        this.longitudParada = longitudParada;
    }

    public ArrayList<String> getLineasQuePasan() {
        return lineasQuePasan;
    }

    public void setLineasQuePasan(ArrayList<String> lineasQuePasan) {
        this.lineasQuePasan = lineasQuePasan;
    }
}
