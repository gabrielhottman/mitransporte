package com.example.mitransporte.ui.login;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mitransporte.MainActivity;
import com.example.mitransporte.R;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory()).get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        loginViewModel.getLoginEstadoFormulario().observe(this, new Observer<LoginEstadoFormulario>() {
            @Override
            public void onChanged(@Nullable LoginEstadoFormulario loginEstadoFormulario) {
                if (loginEstadoFormulario == null) {
                    return;
                }
                loginButton.setEnabled(loginEstadoFormulario.isDataValid());
                if (loginEstadoFormulario.getNombreUsuarioError() != null) {
                    usernameEditText.setError(getString(loginEstadoFormulario.getNombreUsuarioError()));
                }
                if (loginEstadoFormulario.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginEstadoFormulario.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResultado().observe(this, new Observer<LoginResultado>() {
            @Override
            public void onChanged(@Nullable LoginResultado loginResultado) {
                if (loginResultado == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResultado.getError() != null) {
                    showLoginFailed(loginResultado.getError());
                }
                if (loginResultado.getSuccess() != null) {
                    updateUiWithUser(loginResultado.getSuccess());
                }
                setResult(Activity.RESULT_OK);

                //Complete y destruya la actividad de inicio de sesión una vez que tenga éxito
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataModificados(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        });
    }

    private void updateUiWithUser(vistaDeUsuarioConectado model) {
        String welcome = getString(R.string.welcome) + model.getNombreParaMostrar();
        // TODO: inicie una experiencia exitosa de inicio de sesión
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}
