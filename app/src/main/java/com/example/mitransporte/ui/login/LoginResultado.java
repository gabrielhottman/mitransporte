package com.example.mitransporte.ui.login;

import androidx.annotation.Nullable;

/**
    Resultado de autenticación: éxito (detalles del usuario) o mensaje de error.
 */
class LoginResultado {
    @Nullable
    private vistaDeUsuarioConectado success;
    @Nullable
    private Integer error;

    LoginResultado(@Nullable Integer error) {

        this.error = error;
    }

    LoginResultado(@Nullable vistaDeUsuarioConectado success) {

        this.success = success;
    }

    @Nullable
    vistaDeUsuarioConectado getSuccess() {

        return success;
    }

    @Nullable
    Integer getError() {

        return error;
    }
}
