package com.example.mitransporte.ui.login;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.example.mitransporte.data.FuenteDeDatosDeInicioDeSesión;
import com.example.mitransporte.data.LoginRepository;

/**
 * Ver la fábrica de proveedores de modelos para crear una instancia de LoginViewModel.
 * Obligatorio dado que LoginViewModel tiene un constructor no vacío
 */
public class LoginViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(LoginRepository.getInstance(new FuenteDeDatosDeInicioDeSesión()));
        } else {
            throw new IllegalArgumentException("Clase desconocida de ViewModel");
        }
    }
}
