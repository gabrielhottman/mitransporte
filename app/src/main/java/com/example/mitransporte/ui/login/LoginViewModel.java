package com.example.mitransporte.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.util.Patterns;

import com.example.mitransporte.data.LoginRepository;
import com.example.mitransporte.data.Result;
import com.example.mitransporte.data.model.UsuarioConectado;
import com.example.mitransporte.R;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginEstadoFormulario> loginEstadoFormulario = new MutableLiveData<>();
    private MutableLiveData<LoginResultado> loginResult = new MutableLiveData<>();
    private LoginRepository loginRepository;

    LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginEstadoFormulario> getLoginEstadoFormulario() {

        return loginEstadoFormulario;
    }

    LiveData<LoginResultado> getLoginResultado() {

        return loginResult;
    }
    public void login(String username, String password) {
        // se puede iniciar en un trabajo asincrónico separado
        Result<UsuarioConectado> result = loginRepository.login(username, password);

        if (result instanceof Result.Success) {
            UsuarioConectado data = ((Result.Success<UsuarioConectado>) result).getData();
            loginResult.setValue(new LoginResultado(new vistaDeUsuarioConectado(data.getDisplayName())));
        } else {
            loginResult.setValue(new LoginResultado(R.string.login_failed));
        }
    }

    public void loginDataModificados(String username, String password) {
        if (!isUserNameValido(username)) {
            loginEstadoFormulario.setValue(new LoginEstadoFormulario(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginEstadoFormulario.setValue(new LoginEstadoFormulario(null, R.string.invalid_password));
        } else {
            loginEstadoFormulario.setValue(new LoginEstadoFormulario(true));
        }
    }

    // A placeholder verificación de validación de nombre de usuario
    private boolean isUserNameValido(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }
}
