package com.example.mitransporte.ui.login;

import androidx.annotation.Nullable;

/**
 * Estado de validación de datos del formulario de inicio de sesión.
 */
class LoginEstadoFormulario {
    @Nullable
    private Integer nombreUsuarioError;
    @Nullable
    private Integer passwordError;
    private boolean isDataValid;

    LoginEstadoFormulario(@Nullable Integer nombreUsuarioError, @Nullable Integer passwordError) {
        this.nombreUsuarioError = nombreUsuarioError;
        this.passwordError = passwordError;
        this.isDataValid = false;
    }

    LoginEstadoFormulario(boolean isDataValid) {
        this.nombreUsuarioError = null;
        this.passwordError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    Integer getNombreUsuarioError() {

        return nombreUsuarioError;
    }

    @Nullable
    Integer getPasswordError() {

        return passwordError;
    }

    boolean isDataValid() {

        return isDataValid;
    }
}
