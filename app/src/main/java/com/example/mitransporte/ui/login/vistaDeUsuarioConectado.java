package com.example.mitransporte.ui.login;

/**
 * Clase que expone detalles de usuarios autenticados a la Interfaz de Usuario.
 */
class vistaDeUsuarioConectado {
    private String nombreParaMostrar;
    //... otros campos de datos que pueden ser accesibles para la interfaz de usuario

    vistaDeUsuarioConectado(String nombreParaMostrar) {

        this.nombreParaMostrar = nombreParaMostrar;
    }

    String getNombreParaMostrar() {

        return nombreParaMostrar;
    }
}
