package com.example.mitransporte;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.mitransporte.adapter.AdaptadorRecorrido;
import com.example.mitransporte.model.MapaPOO;
import com.example.mitransporte.model.Parada;
import com.example.mitransporte.model.Recorrido;
import com.example.mitransporte.model.Usuario;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.net.URI;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BuscarParadaInvitado extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap googleMap2;

    MapView vistaMapa ;
    Marker marcador, marcadorEstatico, markerparadaMascercana;

    TextView tvNoDisponible;

    FusedLocationProviderClient cliente;
    LocationCallback llamadaUbicacion;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private Boolean mRequestingLocationUpdates;

    LocationManager locationManager;


    String TiempoHora;



    DatabaseReference referenciaBase, referenciaBuses, referenciaParada, referenciaUsuarios ;
    FirebaseAuth autenticacion = FirebaseAuth.getInstance();
    ListView listViewListaLineasBuses;
    ArrayList<Recorrido> listaRecorrido ;

    AdaptadorRecorrido adaptadorRecorrido;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_parada_invitado);

        //getSupportActionBar().hide();

        cliente = LocationServices.getFusedLocationProviderClient(this);


        //mensajeLog("usuario: "+usuario.getUsuario(), "mail: "+ usuario.getCorreo()+" id:"+autenticacion.getCurrentUser().getUid());


        listViewListaLineasBuses = (ListView) findViewById(R.id.listViewListaLineas);

        referenciaBase = FirebaseDatabase.getInstance().getReference();

        tvNoDisponible = (TextView) findViewById(R.id.textViewNoHayItem);


        referenciaBuses =FirebaseDatabase.getInstance().getReference("buses");
        referenciaParada = FirebaseDatabase.getInstance().getReference("Paradas");
        referenciaUsuarios = FirebaseDatabase.getInstance().getReference("Usuarios");

        vistaMapa = findViewById(R.id.miMapa);
        vistaMapa.onCreate(savedInstanceState);
        vistaMapa.getMapAsync(this);

        cargarListaLineasBuses();


        // Inicie el proceso de creación de LocationCallback, LocationRequest y
        // LocationSettingsRequest objetos.
        createLocationCallback();
        createLocationRequest();
        mRequestingLocationUpdates = false;



        //De esta manera se recibe un Objeto Serializable
        //Usuario usuarioRecibidodelLogin = (Usuario) getIntent().getSerializableExtra("objeto");



        //Obtenemos la ActionBar instalada por AppCompatActivity
        ActionBar actionBar = getSupportActionBar();
        //Establecemos el icono en la ActionBar
        actionBar.setIcon(R.mipmap.ic_launcher);
        actionBar.setDisplayShowHomeEnabled(true);


        //Verifica si hay Usuario iniciado Sesion
        if (autenticacion.getCurrentUser() != null){

            startLocationUpdates();


            if (autenticacion.getCurrentUser().getDisplayName().isEmpty() || autenticacion.getCurrentUser().getDisplayName() == null){

                setTitle("Actualice su Perfil");
                actionBar.setSubtitle(autenticacion.getCurrentUser().getEmail());
            }else {
                setTitle(autenticacion.getCurrentUser().getDisplayName());
                actionBar.setSubtitle(autenticacion.getCurrentUser().getEmail());
            }

        }else{
            getSupportActionBar().hide();
            listViewListaLineasBuses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mensajeToast("Debe Iniciar Sesión");
                }
            });
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences preferences = getSharedPreferences("displayName", MODE_PRIVATE);
        SharedPreferences.Editor editor =  preferences.edit();

        boolean hay = preferences.getBoolean("hayCambios", false);
        mensajeLog("onStart: ", String.valueOf(hay));

        editor.putBoolean("hayCambios", false);
        editor.commit();


        if (hay){

            getSupportActionBar().setTitle(autenticacion.getCurrentUser().getDisplayName());
            getSupportActionBar().setSubtitle(autenticacion.getCurrentUser().getEmail());
        }

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Establece el intervalo deseado para las actualizaciones de ubicación activas. Este intervalo es
        // inexacto. Es posible que no reciba actualizaciones si no hay fuentes de ubicación disponibles, o
        // puede recibirlos más lento de lo solicitado. También puede recibir actualizaciones más rápido que
        // solicitado si otras aplicaciones solicitan ubicación en un intervalo más rápido.
        mLocationRequest.setInterval(5000);

        // Establece la tasa más rápida para las actualizaciones de ubicación activas. Este intervalo es exacto y su
        // la aplicación nunca recibirá actualizaciones más rápido que este valor.
        mLocationRequest.setFastestInterval(5000/2);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Crea una devolución de llamada para recibir eventos de ubicación.
     */
    private void createLocationCallback() {
        llamadaUbicacion = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                TiempoHora = DateFormat.getTimeInstance().format(new Date());
                updateLocationUI();
            }
        };
    }

    /**
     * Establece el valor de los campos de IU para la ubicación, latitud, longitud y última hora de actualización.
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {

            double myLatitud = mCurrentLocation.getLatitude();
            double myLongitud = mCurrentLocation.getLongitude();
            mensajeToast("Ubicacion: Lati "+ myLatitud+" Longi "+myLongitud);
        }else{
            mensajeLog("mCurrentLocation", "NULO");
        }
    }

    private void startLocationUpdates() {

        mRequestingLocationUpdates = true;

        if (checkPermissions()){
            mensajeLog("inicia UPDATE", "Fuse starLocationUpdate");
            //sin inspección Falta permiso
            cliente.requestLocationUpdates(mLocationRequest, llamadaUbicacion, Looper.myLooper());

            updateLocationUI();
        }else{
            mensajeLog("No se puede iniciar UPDATE", "Falta Permisos");
        }




    }


    public void cargarListaLineasBuses(){
        String correoInvitado = "gabrielhottman@hotmail.com";
        String contrasenaInvitado = "gabriel1991";


        //autenticacion.signInWithEmailAndPassword(correoInvitado, contrasenaInvitado);
        mensajeLog("path", referenciaBuses.toString());

        listaRecorrido = new ArrayList<Recorrido>();

        referenciaBuses.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String buses = dataSnapshot.getKey();
                Log.e("getKey:Principal ", buses.toString());

                int c=0;
                listaRecorrido.clear();
                if (dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren())
                    {
                        String lineaSinEspaciosMinusculas="";

                        String nombre = snapshot.child("nombreRecorrido").getValue(String.class);
                        String linea = snapshot.child("lineaBus").getValue(String.class);
                        String ruta = snapshot.child("rutaRecorrido").getValue(String.class);
                        String tiempo = snapshot.child("tiempoQuePasaBus").getValue(String.class);

                        c = c+1;
                        mensajeLog("snapshot: # "+ c, linea+" Tiempo: "+tiempo);
//                    String linea = recorrido.getLineaBus();
//                    String nombre = recorrido.getNombreRecorrido();
//                    String ruta = recorrido.getRutaRecorrido();
//                    String tiempo = recorrido.getTiempoQuePasaBus();

                        if (linea != null){
                            lineaSinEspaciosMinusculas = linea.replace(" ", "").toLowerCase().trim();
                        }else {
                            mensajeLog("LINEA", "NULO");
                        }


//                    Log.e("TAG.linea", linea);
//                    Log.e("TAG.recorrido", recorrido);
//                    Log.e("TAG.tiempo", tiempo);

                        int rsc = getResources().getIdentifier(lineaSinEspaciosMinusculas, "drawable", getPackageName());

                        if (nombre != null && ruta != null && linea != null){
                            listaRecorrido.add(new Recorrido(nombre,ruta,"Cada "+tiempo+ " minutos", linea));
                        }

                        // Log.e("Recorrido: ", recorrido.getNombreRecorrido()+" Ruta: "+recorrido.getRutaRecorrido()+" Tiempo: "+recorrido.getTiempoQuePasaBus());

                    }

                    //Sirve Para ordenar Lista Antes de enviar al Adapter
                    Collections.sort(listaRecorrido, new Comparator<Recorrido>() {
                        @Override
                        public int compare(Recorrido o1, Recorrido o2) {
                            String v1 = o1.getLineaBus().toLowerCase().replace(" ", "");
                            String v2 = o2.getLineaBus().toLowerCase().replace(" ", "");
                            return v1.compareTo(v2);
                        }
                    });
                    adaptadorRecorrido = new AdaptadorRecorrido(getApplicationContext(),listaRecorrido);
                    listViewListaLineasBuses.setAdapter(adaptadorRecorrido);

                    adaptadorRecorrido.notifyDataSetChanged();
                    // Log.e("ListaLinea: ", listaRecorrido.toString());
                }else {
                    tvNoDisponible.setVisibility(View.VISIBLE);

                    mensajeLog("datasnapsho", "No existe");
                }





            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("Fallo", "Falló al Leer la Base de Datos.", databaseError.toException());
            }
        });

    }



    Parada paradaCercana;
    float menorDistancia;
    public void buscarParadaMasCercana(){

        final ArrayList<Parada> listaParadas = new ArrayList<>();
        final ArrayList<Float> listaDistancia = new ArrayList<>();

        referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Usuario usuario = dataSnapshot.getValue(Usuario.class);
                    final String lineaUsuario = usuario.getLineaBus();

                    referenciaParada.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                            for (DataSnapshot paradas: dataSnapshot.getChildren()) {
                                Parada parada = paradas.getValue(Parada.class);


                                for (int i=0; i<parada.getLineasQuePasan().size(); i++){
                                    if (lineaUsuario.equals(parada.getLineasQuePasan().get(i))){

                                        Location location = new Location("");
                                        location.setLatitude(mCurrentLocation.getLatitude());
                                        location.setLongitude(mCurrentLocation.getLongitude());


                                        Location location2 = new Location("");
                                        location2.setLatitude(parada.getLatitudParada());
                                        location2.setLongitude(parada.getLongitudParada());

                                        mensajeLog("Esta parada coincide: ",parada.getNombreParada()+" ------- "+parada.getLineasQuePasan().get(i));


                                        float distancia = location.distanceTo(location2);

                                        listaParadas.add(parada);
                                        listaDistancia.add(distancia);
                                        break;
                                    }
                                }
                            }//termina For de las paradas


                            if (listaDistancia.size() != 0){

                                paradaCercana = listaParadas.get(0);
                                menorDistancia = listaDistancia.get(0);

                                for (int i=0; i< listaDistancia.size(); i++){

                                    if (menorDistancia > listaDistancia.get(i)){

                                        paradaCercana = listaParadas.get(i);
                                        menorDistancia = listaDistancia.get(i);
                                    }
                                }
                                mensajeLog("Final Menor Distancia", String.valueOf(menorDistancia));
                                mensajeLog("Final PARADA mas cercana", String.valueOf(paradaCercana.getNombreParada()));
                                mensajeToast("Su Parada Mas Cercana es: "+paradaCercana.getNombreParada());

                                camaraParadaMasCercana(paradaCercana);
                                

                            }else {
                                mensajeLog("No hay Distancias que mostrar", "No Hay Paradas q mostrar");
                                mensajeToast("No Hay paradas para su línea");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                            mensajeLog("BuscandoParadas", "No se pudo Obtener las Paradas mas cercana");
                        }
                    });

                }else {
                    mensajeLog("DataSnapshot", "No existe");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                mensajeLog("Problema","Problema al traer usuarioLinea de Firebase");
            }
        });




    }



    @Override
    public void onMapReady(final GoogleMap googleMap) {

        googleMap2 = googleMap;

        referenciaBase = FirebaseDatabase.getInstance().getReference("geoUbicacionBuses");
        LatLng Manta = new LatLng(-0.9666643, -80.7084783);

        int permiso = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if(permiso == PackageManager.PERMISSION_GRANTED)
        {
            Log.e("Permiso", "Permiso Aceptado");

            googleMap.setMyLocationEnabled(true);
            LocationServices.getFusedLocationProviderClient(this)
                    .getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Es raro que location sea null, pero mejor prevenir
                            if (location != null) {
                                // Aqui ya se donde estoy
                                Double la = location.getLatitude();
                                Double lo = location.getLongitude();
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(la, lo), 16));
                            }
                        }
                    });

            googleMap.addMarker(new MarkerOptions().position(Manta).title("Ciudad de Manta"));



            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Manta, 13));
            referenciaBase.child("Linea 1").child("ECU-911").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    //MapaPOO mapaPOO = dataSnapshot.getValue(MapaPOO.class);

                    Double latitud = Double.valueOf(dataSnapshot.child("Latitud").getValue().toString());
                    Double longitud = Double.valueOf(dataSnapshot.child("Longitud").getValue().toString());

                    MapaPOO mapaPOO = new MapaPOO(latitud, longitud);

                    Log.e("Objeto mapaPOO", mapaPOO.toString());

                    Log.e("Datos Obtenidos", "Latitud: "+latitud+" Longitud: "+longitud);
                    Log.e("Referencia", referenciaBase.toString());


                    MarkerOptions markerOptions = new MarkerOptions();
                    if (marcador != null) marcador.remove();
                    markerOptions.position(new LatLng(latitud, longitud));
                    markerOptions.title("Bus Linea 1");
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icono_bus));

                    marcador = googleMap.addMarker(markerOptions);


                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e("Base", "Error al Leer Datos.");
                }
            });
            //Método que agrega parada de BUS

            Double latitudParada = -0.956059;
            Double longitudParada = -80.745078;
            agregarMarcadorEstatico(latitudParada, longitudParada, googleMap);
            agregarMarcadorEstatico(-0.9577430, -80.74771, googleMap);


            cargarParadasEnElMapa(googleMap);

        }
        else {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, 1);
        }

        //agregarMarcadorEstatico(latitudParada, longitudParada);


    }
    
    public void camaraParadaMasCercana(Parada parada){

        double la = parada.getLatitudParada();
        double lo = parada.getLongitudParada();

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.getTitle();


        markerOptions.position(new LatLng(la,lo));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.parebus2));
        markerOptions.title(parada.getNombreParada());
        markerOptions.snippet("Esta es la Parada Mas cercana...");



        markerparadaMascercana = googleMap2.addMarker(markerOptions);

        markerparadaMascercana.showInfoWindow();

        googleMap2.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(la, lo), 18));
    }


    public void marcadorParadas(Parada parada, GoogleMap googleMap){


        MarkerOptions marcadorParada = new MarkerOptions();
        marcadorParada.position(new LatLng(parada.getLatitudParada(), parada.getLongitudParada()));
        marcadorParada.title("Parada: "+ parada.getNombreParada());
        marcadorParada.icon(BitmapDescriptorFactory.fromResource(R.drawable.parebus2));

        googleMap.addMarker(marcadorParada);

    }
    public  void agregarMarcadorEstatico(double latitud, double longitud, GoogleMap googleMap1){


        MarkerOptions marcadorParada1 = new MarkerOptions();
        marcadorParada1.position(new LatLng(latitud, longitud));
        marcadorParada1.title("Parada de Bus");
        marcadorParada1.icon(BitmapDescriptorFactory.fromResource(R.drawable.parebus2));

        marcadorEstatico = googleMap1.addMarker(marcadorParada1);
    }

    //Metodo que Carga todas las paradas en el Mapa
    public void cargarParadasEnElMapa(final GoogleMap googleMap){
        referenciaParada = FirebaseDatabase.getInstance().getReference("Paradas");

        referenciaParada.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot dato: dataSnapshot.getChildren()) {

                    Parada objetoParada = dato.getValue(Parada.class);

                    mensajeLog("OBJETO PARADA: ", objetoParada.toString());
                    String nombrep = objetoParada.getNombreParada();
                    mensajeLog("NOMBRE PARADA: ", nombrep);

                    marcadorParadas(objetoParada, googleMap);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }







    //MENU ITEM
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        MenuItem itemOculto = menu.findItem(R.id.menuAdministracion);
        if (autenticacion.getCurrentUser().getEmail().equals("root@mail.com") || autenticacion.getCurrentUser().getEmail().equals("admin@yahoo.com")){
            itemOculto.setVisible(true);
        }

        return true;
    }

    //MENU ITEM (Selección)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.menuPerfil:
                intent = new Intent(BuscarParadaInvitado.this, Perfil.class);
                startActivity(intent);
                break;
            case R.id.menuAdministracion:
                intent = new Intent(BuscarParadaInvitado.this, Administracion.class);
                startActivity(intent);
                break;
            case R.id.menuConfiguracion:
                intent = new Intent(BuscarParadaInvitado.this, Configuraciones.class);
                startActivity(intent);
                break;
            case R.id.menuCerrarSesion:
                intent = new Intent(BuscarParadaInvitado.this, MainActivity.class);
                metodoQueCierraSesion();
                finish();
                startActivity(intent);
                break;
            case R.id.menuCAlcula:
                mRequestingLocationUpdates = true;
                buscarParadaMasCercana();
                break;
        }

        return true;
    }



    @Override
    protected void onResume() {
        super.onResume();
        vistaMapa.onResume();

        if (!mRequestingLocationUpdates && checkPermissions() && autenticacion.getCurrentUser() != null){
            mensajeLog("Se reanudan Actualizaciones e Ubicacion", "onResume");
            cliente.requestLocationUpdates(mLocationRequest,
                    llamadaUbicacion, Looper.myLooper()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    mensajeLog("LocationUpdates", "Reanudado, onResume");
                    mRequestingLocationUpdates = true;
                }
            });
        }else{
            mensajeLog("Revise permisos o Boolean", "onResumen");
        }


    }



    @Override
    protected void onPause() {
        super.onPause();
        vistaMapa.onPause();

        if (mRequestingLocationUpdates){
            if (markerparadaMascercana != null){
                markerparadaMascercana.remove();
            }

            cliente.removeLocationUpdates(llamadaUbicacion)
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            mRequestingLocationUpdates = false;
                            mensajeLog("LocationUpdates", "Removido");
//                        mRequestingLocationUpdates = false;
//                        setButtonsEnabledState();
                        }
                    });
        }

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        vistaMapa.onDestroy();
    }






    /**
     * Devuelve el estado actual de los permisos necesarios.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }



    //METODO para Cerrar SESION
    public void metodoQueCierraSesion(){

        autenticacion.signOut();

    }



    public Location obtenerMiUbicacion(){
        cliente = LocationServices.getFusedLocationProviderClient(this);

        final Location[] locationFinal = {null};
        LocationRequest request = new LocationRequest();
        //Especifique con qué frecuencia su aplicación debe solicitar la ubicación del dispositivo//
        request.setInterval(10000);
        //Obtenga los datos de ubicación más precisos disponibles//
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        //final String path = getString(R.string.firebase_path);

        int permiso = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        //Si la aplicación actualmente tiene acceso al permiso de ubicación ...//
        if (permiso == PackageManager.PERMISSION_GRANTED) {
            //... luego solicitar actualizaciones de ubicación//
            cliente.requestLocationUpdates(request, llamadaUbicacion = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    Location localizacion = locationResult.getLastLocation();
                    String direccion = localizacion.toString();

                    Double lactitud = localizacion.getLatitude();
                    Double longitud = localizacion.getLongitude();


                    Toast.makeText(getApplicationContext(), "Lactitud - User : "+ lactitud + " Longitud - User: " + longitud , Toast.LENGTH_SHORT).show();

                    if (localizacion != null) {

                        //Guardar los datos de ubicación en la base de datos//

                        locationFinal[0] = localizacion;
                        mensajeLog("loctaion enviando: ", locationFinal[0].toString());

                    }
                }
            }, null);
        }else {
            locationFinal[0] = null;
            mensajeToast("La app no tiene permisos de ubicación");
        }

        return locationFinal[0];
    }









    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }
}
