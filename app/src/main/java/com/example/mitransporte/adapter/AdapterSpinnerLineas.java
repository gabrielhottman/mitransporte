package com.example.mitransporte.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mitransporte.R;

import java.util.ArrayList;

public class AdapterSpinnerLineas extends BaseAdapter {

    private Context context;
    private ArrayList<String> listaLineas;

    public AdapterSpinnerLineas(Context context, ArrayList<String> listaLineas) {
        this.context = context;
        this.listaLineas = listaLineas;
    }

    @Override
    public int getCount() {
        return listaLineas.size();
    }

    @Override
    public Object getItem(int position) {
        return listaLineas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item_listabus, null);

        TextView txtLinea = convertView.findViewById(R.id.txtLineaSpinnerDisenado);
        ImageView imageView = convertView.findViewById(R.id.imageViewImagenMiniaturaListaSpinner);

        txtLinea.setText(listaLineas.get(position));

        String variableValue = listaLineas.get(position).toLowerCase().replace(" ","");
        Resources resources = context.getResources();
        imageView.setImageResource(resources.getIdentifier(variableValue, "drawable", context.getPackageName()));


        return convertView;
    }
}
