package com.example.mitransporte.adapter;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mitransporte.R;
import com.example.mitransporte.model.Recorrido;

import java.util.ArrayList;

public class AdaptadorRecorrido extends BaseAdapter  {

    Context context;
    ArrayList<Recorrido> listaRecorrido;

    public AdaptadorRecorrido(Context context, ArrayList<Recorrido> listaRecorrido) {
        this.context = context;
        this.listaRecorrido = listaRecorrido;
    }

    @Override
    public int getCount() {

        return listaRecorrido.size();
    }

    @Override
    public Object getItem(int position) {
        return listaRecorrido.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.item_lista_lineas, null);

        TextView linea = convertView.findViewById(R.id.textViewLinea);
        TextView ruta = convertView.findViewById(R.id.textViewRecorrido);
        TextView tiempo = convertView.findViewById(R.id.textViewTiempoQuePasa);
        ImageView imagen = convertView.findViewById(R.id.imageViewBus);

        linea.setText(listaRecorrido.get(position).getLineaBus());
        ruta.setText(listaRecorrido.get(position).getRutaRecorrido());
        tiempo.setText(listaRecorrido.get(position).getTiempoQuePasaBus());

        String img = listaRecorrido.get(position).getLineaBus().toLowerCase();
        String valorFinal = img.replace(" ", "");

        //int images = R.drawables.valorFinal;
        Resources res = context.getResources();
        int resID = res.getIdentifier(valorFinal, "drawable", context.getPackageName());

        imagen.setImageResource(resID);


        return convertView;
    }
}
