package com.example.mitransporte;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.mitransporte.model.Usuario;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class PantallaInicio extends AppCompatActivity {

    FirebaseAuth autenticate;
    FirebaseAuth.AuthStateListener authStateListener;

    //Constante de tipo entero que representa el tiempo en
    //millisegundos que se muestra actividad al usuario
    static int TIMEOUT_MILLIS = 2000;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Asignamos el archivo xml que va a ligarse como diseño de la pantalla
        setContentView(R.layout.activity_pantalla_inicio);

        //Quitamos el action bar de la pantalla Splash
        getSupportActionBar().hide();

        mensajeLog("inicia ","onCreate");

        autenticate = FirebaseAuth.getInstance();


        authStateListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null){
                    mensajeLog("ListenerAuh: Usuario", firebaseAuth.getCurrentUser().getEmail());

                }else{

                    mensajeLog("ListenerAuh", "No Hay Usuario Logueado");
                }

            }

        };


        autenticate.addAuthStateListener(authStateListener);

        delayPantallaDeInicio();

    }



    @Override
    protected void onStart() {
        super.onStart();

        if (autenticate.getCurrentUser() != null){
            mensajeLog("onStart: ", "userHastaAqu: "+autenticate.getCurrentUser());
        }else {
            mensajeLog("onStart: NULO USER", "NULL");
        }
        //autenticate.removeAuthStateListener(authStateListener);

    }

    public void delayPantallaDeInicio(){
        //La clase Handle permite enviar objetos de un tipo Runnable.
        //Un objeto de tipo unnable dá el significado a un hilo de proceso
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent es la definicion abstarcta de una operación a realizar
                //Puede ser usado parauna activity, broadcast receiver, servicios.


                if (autenticate.getCurrentUser() != null){
                    Intent i = new Intent(PantallaInicio.this, BuscarParadaInvitado.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(PantallaInicio.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, TIMEOUT_MILLIS);
    }


    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }


}
