package com.example.mitransporte;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mitransporte.gestiones.RegistroBus;
import com.example.mitransporte.gestiones.RegistroParadas;
import com.example.mitransporte.gestiones.RegistroRecorridos;

public class Administracion extends AppCompatActivity {

    Button btnRegistrarBusNuevo, btnRegistroParada, btnRegistrarRecorrido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administracion);
        getSupportActionBar().hide();

        btnRegistrarBusNuevo = (Button) findViewById(R.id.btnRegistrarBusNuevo);
        btnRegistroParada = (Button) findViewById(R.id.btnRegistrarParada);
        btnRegistrarRecorrido = (Button) findViewById(R.id.btnRegistrarRecorrido);
    }

    public void registraBusNuevo (View view){
        Intent registroBus = new Intent(this, RegistroBus.class);
        startActivity(registroBus);
    }
    public void registraParada (View view){
        Intent registroParada = new Intent(this, RegistroParadas.class);
        startActivity(registroParada);
    }

    public void registraRecorrido (View view){
        Intent registroRecorrido = new Intent(this, RegistroRecorridos.class);
        startActivity(registroRecorrido);
    }
}
