package com.example.mitransporte.gestiones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mitransporte.LoginPrincipal;
import com.example.mitransporte.MainActivity;
import com.example.mitransporte.R;
import com.example.mitransporte.adapter.AdapterSpinnerLineas;
import com.example.mitransporte.model.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RegistroUsuarios extends AppCompatActivity {

    EditText txtUsuario, txtCorreo, txtTelefono, txtContrasena ;
    TextView labelIngresar;
    Button btnRegistrar;
    Spinner spinnerLineaDeBus;

    ImageView imageViewCompletado;
    LinearLayout linearLayout1;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String lineaBus;


    FirebaseAuth autenticacion;
    DatabaseReference referenciaBaseUsuarios, referenciaBaseBuses;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuarios);
        getSupportActionBar().hide();


        autenticacion = FirebaseAuth.getInstance();
        referenciaBaseUsuarios = FirebaseDatabase.getInstance().getReference("Usuarios");
        referenciaBaseBuses = FirebaseDatabase.getInstance().getReference("buses");


        labelIngresar = findViewById(R.id.textViewIngrsar);


        labelIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistroUsuarios.this, LoginPrincipal.class);
                startActivity(i);
                finish();
            }
        });

        linearLayout1 = (LinearLayout) findViewById(R.id.linearLayout1);

        txtTelefono = (EditText) findViewById(R.id.txtTelefono);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtContrasena = (EditText) findViewById(R.id.txtContrasena);
        txtCorreo = (EditText) findViewById(R.id.txtCorreo);
        spinnerLineaDeBus = (Spinner) findViewById(R.id.spinnerLineaDeBus);
        imageViewCompletado = (ImageView) findViewById(R.id.imageViewCompletado);

        btnRegistrar= (Button) findViewById(R.id .btnRegistrar);


        cargarLineasDeFirebase();

    }


    //cargar lineas de la base
    public void cargarLineasDeFirebase(){

        final ArrayList<String> listaLineas = new ArrayList<>();

        referenciaBaseBuses.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot dato: dataSnapshot.getChildren()){
                        listaLineas.add(dato.getKey());
                    }

                    AdapterSpinnerLineas adapter = new AdapterSpinnerLineas(getApplicationContext(), listaLineas);
                    spinnerLineaDeBus.setAdapter(adapter);

                }else{
                    mensajeLog("NO EXISTE", "dataSnapshot, registroUsuario-cargarLineasDeFirebas");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                mensajeLog("Problema Firebase", databaseError.getDetails());
            }
        });

    }


    //Aqui me conecto a la firebase y verifico que no estan repetidos el user o mail
    //Una vez verifico que no hayan sido registrados envio al metodo crearUsuarioNuevo, el usuario, contraseña y objeto USUARIO
    public void RegistrarUsuario(View view){


        mensajeLog("void RegistroUsuario", "Inicia");
        if (verificaCamposVacios()){

            final String usuarioNombre = txtUsuario.getText().toString();
            final String correo = txtCorreo.getText().toString();
            String telefono = txtTelefono.getText().toString();
            lineaBus = spinnerLineaDeBus.getSelectedItem().toString();

            final String contrasena = txtContrasena.getText().toString();

            final Usuario usuario = new Usuario(usuarioNombre,correo, telefono, lineaBus);

            referenciaBaseUsuarios.addListenerForSingleValueEvent(new ValueEventListener() {
                Usuario user1;
                String userRepetido = null, mailRepetido = null;
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot dato: dataSnapshot.getChildren()) {
//
                        user1 = dato.getValue(Usuario.class);

                        if (usuarioNombre.equals(user1.getUsuario())){
                            userRepetido = user1.getUsuario();
                            break;
                        }else if (correo.equals(user1.getCorreo())){

                            mailRepetido = user1.getCorreo();
                            break;
                        }else {

                            mensajeLog("Ingresa", "No está registrado");
                        }
                    }
                    if (userRepetido != null){
                        mensajeToast("Este Usuario ya está registrado");
                    }else if (mailRepetido != null){
                        mensajeToast("Este mail ya está registrado");
                    }else {
                        mensajeLog("USUARIO: ", usuario.getUsuario());
                        mensajeLog("CORREO: ", usuario.getCorreo());

                        creaUsuarioNuevo(usuario.getCorreo(), contrasena, usuario);
                    }
                    //NO GUARDA
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                    Log.w("Eroor", "No funciona", databaseError.toException());
                }
            });
        }
    }



    //Aqui recivo user, pass y objt USUARIO una vez hayan sido validados en el metodo anterior y posteriormente creo un Usuario nuevo
    public void creaUsuarioNuevo(final String correo, final String contrasena, final Usuario usuario){

        autenticacion.createUserWithEmailAndPassword(correo, contrasena).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    mensajeLog("createUserWithEmailAndPassword", "Exitoso!!");

                    FirebaseUser user = autenticacion.getCurrentUser();

                    //Una vez creamos el usuario de Firebase le actualizamos el displayName co el nombre de Usuario
                    UserProfileChangeRequest actualizaUsuarioFirebase = new UserProfileChangeRequest.Builder()
                            .setDisplayName(usuario.getUsuario()).build();
                    autenticacion.getCurrentUser().updateProfile(actualizaUsuarioFirebase).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                mensajeLog("updateProfile", "displayName actualizado correctamente");
                            }else{
                                mensajeLog("updateProfile", "displayName: No se pudo Realizar");
                            }
                        }
                    });

                    //despues de actualizar el displayName del usuario de firebase seguimos a crear demas campos de usuario en
                    //la bese de datos nuestra de firebase (Enviamos objeto usuario con todos sus atributos)
                    referenciaBaseUsuarios.child(user.getUid()).setValue(usuario).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                mensajeToast("Usuario Creado Exitosamente: Base de Datos RealTime");

                                autenticacion.signOut();
                                MainActivity.actividadMain.finish();

                                ocultarCamposRegistroUsuario();
                                delayCrearUsuario();
                            }else{
                                mensajeLog("Usuario Incompleto: ", "Usuario de Firebase creado, pero no se pudo agregar la info en la base de datos de Firebase");
                            }
                        }
                    });

                }else{
                    mensajeLog("Problema","No se pudo crear el usuario");
                }

            }
        });
        autenticacion.signOut();

        if (autenticacion.getCurrentUser() != null){
            mensajeLog("Usuario Actual:------------------------------------> ", autenticacion.getCurrentUser().getEmail());
        }else{
            mensajeLog("Usuario Actual:------------------------------------> ", "No hay usuario Logueado");
        }

    }



    public boolean verificaCamposVacios(){
        if (TextUtils.isEmpty(txtUsuario.getText().toString().trim())){
            mensajeToast("Debe escribir un Nombre de Usuario");
            txtUsuario.requestFocus();
            return false;
        }else if (txtCorreo.getText().toString().trim().isEmpty()){
            mensajeToast("Debe escribir un Correo");
            txtCorreo.requestFocus();
            return false;
        }else if (TextUtils.isEmpty(txtContrasena.getText())){
            mensajeToast("Debe escribir una contraseña");
            txtContrasena.requestFocus();
            return false;
        }else if (txtContrasena.length() < 6){
            mensajeToast("La contraseña debe tener al menos 6 caracteres");
            txtContrasena.requestFocus();
            return false;
        }else if (!TextUtils.isEmpty(txtTelefono.getText())){
            if (txtTelefono.length() != 10){
                mensajeToast("El Telefono debe contener 10 números");
                txtTelefono.requestFocus();
                return false;
            }
        }else if (!txtCorreo.getText().toString().matches(emailPattern)){
            mensajeToast("Formato de Correo Inválido");
            txtCorreo.requestFocus();
            return false;
        }
            return true;


    }

    public void ocultarCamposRegistroUsuario(){

//        txtNombre.setVisibility(View.INVISIBLE);
//        txtCorreo.setVisibility(View.INVISIBLE);
//        txtContrasena.setVisibility(View.INVISIBLE);
//        txtUsuario.setVisibility(View.INVISIBLE);
//        spinnerLineaDeBus.setVisibility(View.INVISIBLE);
            linearLayout1.setVisibility(View.INVISIBLE);
       // btnRegistrar.setVisibility(View.INVISIBLE);
        imageViewCompletado.setVisibility(View.VISIBLE);

    }

    public void limpiarCamposRegistroUsuario(){
        txtTelefono.getText().clear();
        txtCorreo.getText().clear();
        txtContrasena.getText().clear();
        txtUsuario.getText().clear();
    }

    public void delayCrearUsuario(){
        //Quitamos el action bar de la pantalla Splash

        //getSupportActionBar().hide();

        //La clase Handle permite enviar objetos de un tipo Runnable.
        //Un objeto de tipo unnable dá el significado a un hilo de proceso


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent es la definicion abstarcta de una operación a realizar
                //Puede ser usado parauna activity, broadcast receiver, servicios.
                Intent i = new Intent(RegistroUsuarios.this, LoginPrincipal.class);
                startActivity(i);
                //Finaliza esta activity
                finish();
            }
        }, 2500);
    }





    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }
}
