package com.example.mitransporte.gestiones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mitransporte.Administracion;
import com.example.mitransporte.BuscarParadaInvitado;
import com.example.mitransporte.R;
import com.example.mitransporte.model.Recorrido;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistroRecorridos extends AppCompatActivity {

    EditText txtNombreRecorrido, txtRutaRecorrido, txtTiempoQuePasaBus;
    ImageView imageViewRegistroRecorridoCompletado;
    Spinner spLinea;
    Button btnRegistrarRecorrido;

    FirebaseDatabase instanciaDataBase = FirebaseDatabase.getInstance();
    DatabaseReference referenciaBase = instanciaDataBase.getReference("buses");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_recorridos);
        getSupportActionBar().hide();

        String [] listaLineas = {"Linea 1", "Linea 2", "Linea 3", "Linea 4", "Linea 5", "Linea 6", "Linea 7",
                "Linea 8", "Linea 9", "Linea 10", "Linea 11", "Linea 12", "Linea 13", "Linea 14", "Linea 15", "Linea 16", "Linea 17"};

        txtNombreRecorrido = findViewById(R.id.txtNombreRecorrido);
        txtRutaRecorrido = findViewById(R.id.txtRutaRecorrido);
        txtTiempoQuePasaBus = findViewById(R.id.txtTiempoQuePasaBus);
        btnRegistrarRecorrido = findViewById(R.id.btnRegistrarRecorrido);
        imageViewRegistroRecorridoCompletado = findViewById(R.id.imageViewCompletadoRegistroRecorrido);
        spLinea = findViewById(R.id.spinnerLineaDeRecorrido);

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, listaLineas);
        spLinea.setAdapter(adapter);



        btnRegistrarRecorrido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                botonRegistrarRecorrido();
            }
        });


    }

    public void botonRegistrarRecorrido(){
        String linea = spLinea.getSelectedItem().toString();
        String nombreRecorrido = txtNombreRecorrido.getText().toString();
        String rutaRecorrido = txtRutaRecorrido.getText().toString();
        String tiempoQuePasaBus = txtTiempoQuePasaBus.getText().toString();


        if (nombreRecorrido.isEmpty() || nombreRecorrido == null){
            mensajeToast("Debe Ingresar un Nombre");
        }else if (rutaRecorrido.isEmpty() || rutaRecorrido == null){
            mensajeToast("Debe Ingresar un Recorrido");
        }else if (tiempoQuePasaBus.isEmpty() || tiempoQuePasaBus == null){
            mensajeToast("Debe Ingresar un tiempo");
        }else {
            Recorrido recorrido = new Recorrido(nombreRecorrido, rutaRecorrido, tiempoQuePasaBus, linea);
            referenciaBase.child(linea).child("lineaBus").setValue(recorrido.getLineaBus());
            referenciaBase.child(linea).child("nombreRecorrido").setValue(recorrido.getNombreRecorrido());
            referenciaBase.child(linea).child("rutaRecorrido").setValue(recorrido.getRutaRecorrido());
            referenciaBase.child(linea).child("tiempoQuePasaBus").setValue(recorrido.getTiempoQuePasaBus());
            mensajeToast("Recorrido Agregado Exitosamente");
            limpiarCamposRegistroRecorrido();
            delayCrearRecorrido();
        }
    }

    public void limpiarCamposRegistroRecorrido(){
        txtNombreRecorrido.getText().clear();
        txtRutaRecorrido.getText().clear();
        txtTiempoQuePasaBus.getText().clear();
        imageViewRegistroRecorridoCompletado.setVisibility(View.VISIBLE);
        //Oculta los EditText y solo deja la img de "Completado Correctamente"
        txtTiempoQuePasaBus.setVisibility(View.INVISIBLE);
        txtRutaRecorrido.setVisibility(View.INVISIBLE);
        txtNombreRecorrido.setVisibility(View.INVISIBLE);
        spLinea.setVisibility(View.INVISIBLE);
        btnRegistrarRecorrido.setVisibility(View.INVISIBLE);

    }
    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }

    private void delayCrearRecorrido() {
        //Quitamos el action bar de la pantalla Splash
        getSupportActionBar().hide();
        //La clase Handle permite enviar objetos de un tipo Runnable.
        //Un objeto de tipo unnable dá el significado a un hilo de proceso


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent es la definicion abstarcta de una operación a realizar
                //Puede ser usado parauna activity, broadcast receiver, servicios.
                Intent i = new Intent(RegistroRecorridos.this, Administracion.class);
                startActivity(i);
                //Finaliza esta activity
                finish();
            }
        }, 2500);
    }
}
