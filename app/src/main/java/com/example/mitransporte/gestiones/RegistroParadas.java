package com.example.mitransporte.gestiones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mitransporte.Administracion;
import com.example.mitransporte.R;
import com.example.mitransporte.adapter.AdapterSpinnerLineas;
import com.example.mitransporte.model.Parada;
import com.example.mitransporte.model.Recorrido;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RegistroParadas extends AppCompatActivity{

    ArrayList<String> listaLineasDeBuses;


    boolean [] checkedItems;

    String [] listaDeItems;
    ArrayList<String> listaUserItems =new ArrayList<>();


    EditText txtNombreParada, txtLatitudParada, txtLongitudParada, txtSectorParada;

    EditText txtLineasQuePasan;
    TextView tvLineasQuePasan;
    Button btnRegistrarParada;

    ImageView imagenRegistroExitosoParada;

    DatabaseReference referenciaBaseParadas, referenciaBaseBuses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_paradas);
        getSupportActionBar().hide();

        referenciaBaseParadas = FirebaseDatabase.getInstance().getReference("Paradas");

        referenciaBaseBuses = FirebaseDatabase.getInstance().getReference("buses");


        listaLineasDeBuses = new ArrayList<>();






        referenciaBaseBuses.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot dato: dataSnapshot.getChildren()){
                        listaLineasDeBuses.add(dato.getKey());
                    }

                    listaDeItems = listaLineasDeBuses.toArray(new String[listaLineasDeBuses.size()]);

                    checkedItems = new boolean[listaDeItems.length];

                    mensajeLog("listaDeItems|||", listaDeItems.toString());
                    mensajeLog("checkedItems", checkedItems.toString());
                    mensajeLog("listaLineasDeBuses", listaLineasDeBuses.toString());
                }else{
                    mensajeLog("NO EXISTE", "dataSnapshot, registroUsuario-cargarLineasDeFirebas");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                mensajeLog("Problema Firebase", databaseError.getDetails());
            }
        });





        imagenRegistroExitosoParada = findViewById(R.id.imageViewCompletadoRegistroParada);

        txtNombreParada = findViewById(R.id.txtNombreDeParada);
        txtLatitudParada = findViewById(R.id.txtLatitudParada);
        txtLongitudParada = findViewById(R.id.txtLongitudParada);
        btnRegistrarParada = findViewById(R.id.btnRegistrarParada);
        txtSectorParada = findViewById(R.id.txtSectorDeParada);

        txtLineasQuePasan = (EditText) findViewById(R.id.txtLineasQuePasan);
        txtLineasQuePasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkedItems != null){
                    metodoSeleccionarLineasQuepasan();
                }else {
                    mensajeToast("Aún no hay lineas disponibles.");
                }

            }
        });

        tvLineasQuePasan = (TextView) findViewById(R.id.textViewLineaBus);
        tvLineasQuePasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedItems != null){
                    metodoSeleccionarLineasQuepasan();
                }else {
                    mensajeToast("Aún no hay lineas disponibles.");
                }
            }
        });


        btnRegistrarParada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                botonRegistroParada();
            }
        });

    }



    //Alert Dialog
    private void metodoSeleccionarLineasQuepasan(){
        mensajeLog("presionado/", "Lineas q pasan");
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);

        alertBuilder.setTitle("Seleccione las lineas que pasan.");
        alertBuilder.setMultiChoiceItems(listaDeItems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                //Esto es para que empiece desde 1 y no de cero
                int posision = which+1;
                if (isChecked){
                    if (!listaUserItems.contains(listaDeItems[which])){
                        listaUserItems.add(listaDeItems[which]);
                    }else {

                    }
                }else{
                    mensajeLog("No chequeado, ", "Deseleccionado");
                    if (listaUserItems.contains(listaDeItems[which])){
                        listaUserItems.remove(listaDeItems[which]);
                    }
                }
                //Ordemanos la lista de string
                Collections.sort(listaUserItems);
            }
        });
        alertBuilder.setCancelable(false);
        alertBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String item = "";

                for (int i = 0; i < listaUserItems.size(); i++){
                    item = item + listaUserItems.get(i);
                    if (i != listaUserItems.size() - 1){
                        item = item + ", ";
                    }
                }
                mensajeLog("Lista contiene: btn Positivo", listaUserItems.toString());
                txtLineasQuePasan.setText(item);
            }
        });
        alertBuilder.setNeutralButton("Limpiar ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (checkedItems != null){
                    for (int i = 0; i<checkedItems.length ; i++){
                        checkedItems[i] = false;
                        listaUserItems.clear();
                        txtLineasQuePasan.setText("");
                    }
                }else {

                    mensajeLog("checkedItems", "VACIO");
                }

                mensajeLog("Lista contiene: Limpiar", listaUserItems.toString());
            }
        });
        AlertDialog alerta = alertBuilder.create();
        alerta.show();
    }




    public void botonRegistroParada(){
        String nombreParada = txtNombreParada.getText().toString();
        String sectorParada = txtSectorParada.getText().toString();
        String latitud = txtLatitudParada.getText().toString();
        String longitud = txtLongitudParada.getText().toString();
        String lineaQuePasaParada = txtLineasQuePasan.getText().toString();



        if (nombreParada.isEmpty() || nombreParada == null){
            mensajeToast("Debe Ingresar un Nombre");
        }else if (sectorParada.isEmpty() || sectorParada == null){
            mensajeToast("Debe Ingresar un Sector\nBarrio - Ciudadela");
        }else if (latitud.isEmpty() || latitud == null){
            mensajeToast("Debe Ingresar una Latitud");
        }else if (longitud.isEmpty() || longitud == null){
            mensajeToast("Debe Ingresar una Longitud");
        }else if (lineaQuePasaParada.isEmpty() || lineaQuePasaParada == null){
            mensajeToast("Debe Ingresar al menos una Linea");
        }else {
            double latitudParada = Double.parseDouble(latitud);
            double longitudParada = Double.parseDouble(longitud);

            Parada parada = new Parada(nombreParada, sectorParada, latitudParada, longitudParada, listaUserItems);
            referenciaBaseParadas.child(sectorParada).setValue(parada);
            mensajeToast("Parada Agregada Exitosamente");
            limpiarCamposRegistroParada();
            delayCrearParada();
        }
    }

    private void limpiarCamposRegistroParada() {

        imagenRegistroExitosoParada.setVisibility(View.VISIBLE);
        //Oculta los EditText y solo deja la img de "Completado Correctamente"
        txtNombreParada.setVisibility(View.INVISIBLE);
        txtSectorParada.setVisibility(View.INVISIBLE);
        txtLineasQuePasan.setVisibility(View.INVISIBLE);
        txtLatitudParada.setVisibility(View.INVISIBLE);
        txtLongitudParada.setVisibility(View.INVISIBLE);
        btnRegistrarParada.setVisibility(View.INVISIBLE);
    }









    private void delayCrearParada() {
        //Quitamos el action bar de la pantalla Splash
        getSupportActionBar().hide();
        //La clase Handle permite enviar objetos de un tipo Runnable.
        //Un objeto de tipo unnable dá el significado a un hilo de proceso


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent es la definicion abstarcta de una operación a realizar
                //Puede ser usado parauna activity, broadcast receiver, servicios.
                Intent i = new Intent(RegistroParadas.this, Administracion.class);
                startActivity(i);
                //Finaliza esta activity
                finish();
            }
        }, 2500);
    }

    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }

}
