package com.example.mitransporte.gestiones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mitransporte.R;
import com.example.mitransporte.adapter.AdapterSpinnerLineas;
import com.example.mitransporte.model.Bus;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RegistroBus extends AppCompatActivity {

    DatabaseReference referenciaBaseBuses;

    Spinner spinnerLineaDeBus;
    EditText txtPlacaDeBus, txtRecorridoBus, txtChoferBus, txtAnioDeUsoBus, txtCooperativaBus;
    Button btnRegistrarBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_bus);
        getSupportActionBar().hide();

        referenciaBaseBuses = FirebaseDatabase.getInstance().getReference("buses");


        spinnerLineaDeBus = (Spinner) findViewById(R.id.spinnerLineaRegistroBus);
        //este spiner se va a cargar en el metodo cargarLineasDeFirebase()
        cargarLineasDeFirebase();

        txtPlacaDeBus = (EditText) findViewById(R.id.txtPlacaBus);
        txtChoferBus = (EditText) findViewById(R.id.txtChoferBus);
        txtAnioDeUsoBus = (EditText) findViewById(R.id.txtAnioDeUsoBus);
        txtCooperativaBus = (EditText) findViewById(R.id.txtCooperativaBus);
        btnRegistrarBus = (Button) findViewById(R.id.btnRegistrarBus);
        btnRegistrarBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                botonRegistrarBus();
            }
        });
    }

    //cargar lineas de la base y agregarlas al Spinner
    public void cargarLineasDeFirebase(){

        final ArrayList<String> listaLineas = new ArrayList<>();

        referenciaBaseBuses.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot dato: dataSnapshot.getChildren()){
                        listaLineas.add(dato.getKey());
                    }

                    AdapterSpinnerLineas adapter = new AdapterSpinnerLineas(getApplicationContext(), listaLineas);
                    spinnerLineaDeBus.setAdapter(adapter);

                }else{
                    mensajeLog("NO EXISTE", "dataSnapshot, registroUsuario-cargarLineasDeFirebas");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                mensajeLog("Problema Firebase", databaseError.getDetails());
            }
        });

    }


    public void botonRegistrarBus(){

        String valorLineaSpinner = spinnerLineaDeBus.getSelectedItem().toString();
        final String placa = txtPlacaDeBus.getText().toString();
        String chofer = txtChoferBus.getText().toString();
        String anio = txtAnioDeUsoBus.getText().toString();
        String coop = txtCooperativaBus.getText().toString();

        if (placa.isEmpty()||placa == null){
            mensajeToast("Debe escribir una Placa");
        }else if (chofer.isEmpty()||chofer == null){
            mensajeToast("Debe escribir un nombre de Chofer");
        }else if (anio.isEmpty()||anio == null){
            mensajeToast("Debe escribir los años de uso");
        }else if (coop.isEmpty()||coop == null){
            mensajeToast("Escriba una Cooperativa");
        }else {

            String lineaSinEspaciosMinusculas = valorLineaSpinner.trim().toLowerCase().replace(" ", "");

            mensajeLog("referencia", referenciaBaseBuses.toString());
            mensajeLog("lineaSinEspaciosMinusculas", lineaSinEspaciosMinusculas);
            int foto = getResources().getIdentifier(lineaSinEspaciosMinusculas, "drawable", getPackageName());
            mensajeLog("Foto: ", String.valueOf(foto));

            Bus busNuevo = new Bus(foto, valorLineaSpinner,chofer,placa,anio,coop);

            referenciaBaseBuses.child(valorLineaSpinner).child(placa).setValue(busNuevo);

            limpiarCampos();

            mensajeToast("RegistroUsuarios Exitoso");
            mensajeLog("BusNuevo: ", busNuevo.toString());
        }

    }

    public void limpiarCampos(){
        txtPlacaDeBus.getText().clear();
        txtChoferBus.getText().clear();
        txtAnioDeUsoBus.getText().clear();
        txtCooperativaBus.getText().clear();


    }
    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }

}
