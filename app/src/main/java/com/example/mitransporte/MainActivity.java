package com.example.mitransporte;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mitransporte.gestiones.RegistroBus;
import com.example.mitransporte.gestiones.RegistroParadas;
import com.example.mitransporte.gestiones.RegistroRecorridos;
import com.example.mitransporte.gestiones.RegistroUsuarios;
import com.example.mitransporte.model.Usuario;
import com.example.mitransporte.ui.login.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    public static MainActivity actividadMain;

    Button btnBuscarParada, btnRegistrar, btnIngresar;
    FirebaseAuth autenticacion;
    DatabaseReference referenciaBaseUsuarios;

    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        actividadMain = this;


        autenticacion =  FirebaseAuth.getInstance();
        referenciaBaseUsuarios = FirebaseDatabase.getInstance().getReference("Usuarios");

        btnBuscarParada = (Button) findViewById(R.id.btnBuscarParadaInvitado);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);

        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        if (estaHabilitadoProveedorUbicacion()){
            mensajeLog("provider: ", "Activado");
        }else {
            mensajeLog("provider: ", "DESACTIVADO");
        }

        compruebaGpsActivado();

        compruebaPermisosDeUbicacion();


    }

    public boolean compruebaPermisosDeUbicacion(){
        int permiso = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permiso == PackageManager.PERMISSION_GRANTED){
            mensajeLog("Permiso ACSESFINELOCATON", "Concebido");

            return true;
        }else {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, 1);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // Si se ha otorgado el permiso ... //
        if (requestCode == 1 && grantResults.length == 1 && grantResults [ 0 ] == PackageManager.PERMISSION_GRANTED) {

        } else {
            // Si el usuario rechaza la solicitud de permiso, muestre un brindis con más información //
            Toast.makeText ( this, "Debe activar los permisos de UBICACIÓN" , Toast.LENGTH_SHORT) .show ();
            finish();
        }
    }

    public void buscarParada(View view){
        Intent buscarParada = new Intent(this, BuscarParadaInvitado.class);
        startActivity(buscarParada);
    }

    public void registrarse(View view){
        Intent registro = new Intent(this, RegistroUsuarios.class);
        startActivity(registro);
    }

    public void Ingresar (View view){
        Intent registro = new Intent(this, LoginPrincipal.class);
        startActivity(registro);
    }



    @Override
    protected void onStart() {
        super.onStart();

        mensajeLog("MainActivity: ", "OnStart");
        if (autenticacion.getCurrentUser() != null){

            mensajeLog("HAY USUARIO LOGUEADO: ", "Main Ativity");
            getDatosUsuarioLogueado(autenticacion.getCurrentUser().getUid());
            mensajeLog("Datos USER: ", autenticacion.getCurrentUser().getUid());

        }else{
            mensajeLog("NINGUN USUARIO: ", "Main Ativity");
        }
    }

    public void getDatosUsuarioLogueado(String UID){

        String uidUsuarioLogueado = UID;
        mensajeLog("Datos USER: ", autenticacion.getCurrentUser().getEmail());
        referenciaBaseUsuarios.child(uidUsuarioLogueado).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mensajeLog("Datos USER: ", dataSnapshot.toString());
                if (dataSnapshot.exists()){
                    mensajeLog("OnDataChange ", "Entra");
                    Usuario usuario = dataSnapshot.getValue(Usuario.class);
                    mensajeLog("Esto importé: ", usuario.getUsuario());
                    mensajeLog("Esto importé: ", usuario.getCorreo());
                    mensajeLog("Esto importé: ", usuario.getTelefono());
                    mensajeLog("Esto importé: ", usuario.getLineaBus());
                    Intent i = new Intent(MainActivity.this, BuscarParadaInvitado.class);
                    i.putExtra("objeto", usuario);
                    startActivity(i);
                    finish();
                }else{
                    mensajeLog("DataSnapshot: ", "No existe");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mensajeLog("MainActivity", "Error, no se pudo extraer info Usuario de Firebase");
            }
        });
    }



    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }

    public boolean estaHabilitadoProveedorUbicacion(){
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    public boolean compruebaGpsActivado(){
        // Comprueba si el rastreo GPS está habilitado //
        LocationManager lm = (LocationManager) getSystemService (LOCATION_SERVICE);
        if (!lm.isProviderEnabled (LocationManager.GPS_PROVIDER)){
            mensajeToast("El GPS esta Desactivado, Activelo por favor");
            mostrarAlertaActivarGPS();
            return false;
        }
        else{
            mensajeToast("GPS Activado");
            return true;
            //compruebaPermisosUbicacion();
        }
    }

    private void mostrarAlertaActivarGPS() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Active la Ubicación")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación " +
                        "usa esta app")
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        mensajeToast("Debe Activar el GPS");
                        finish();
                    }
                });
        dialog.show();
    }
}
