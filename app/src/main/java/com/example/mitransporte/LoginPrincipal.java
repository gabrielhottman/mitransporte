package com.example.mitransporte;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mitransporte.gestiones.RegistroUsuarios;
import com.example.mitransporte.model.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.net.URI;

public class LoginPrincipal extends AppCompatActivity {

    EditText txtLoginUsuario, txtLoginContrasena;
    TextView labelRegistrate;
    Button btnLoginIngresar;
    private FirebaseAuth autenticacion;
    DatabaseReference referenciaBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_principal);

        getSupportActionBar().hide();

        referenciaBase = FirebaseDatabase.getInstance().getReference("Usuarios");
        autenticacion = FirebaseAuth.getInstance();

        txtLoginUsuario = findViewById(R.id.txtLoginUsuario);
        txtLoginContrasena = findViewById(R.id.txtLoginContrasena);
        btnLoginIngresar = findViewById(R.id.btnIngresarLogin);
        btnLoginIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                botonIngresar();
            }
        });

        labelRegistrate = findViewById(R.id.textViewRegistrate);

        labelRegistrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickLabelRegistrate();
            }
        });
    }


    public void verificaconexionNetworkActiva(){
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(this.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }


    public void clickLabelRegistrate(){
        Intent i = new Intent(LoginPrincipal.this, RegistroUsuarios.class);
        startActivity(i);
        finish();
    }

    public void botonIngresar(){

        final String user = txtLoginUsuario.getText().toString().trim();
        String pass= txtLoginContrasena.getText().toString().trim();

        mensajeLog("Entrada - User",user);
        mensajeLog("Entrada - Pass",pass);

        if (user.trim().isEmpty()){
            mensajeToast("Ingrese un Usuario");
        }else if (pass.isEmpty()){
            mensajeToast("Ingrese una Contraseña");
        }else if (pass.length() < 6){
            mensajeToast("Mínimo de 6 Carácteres debe escribir");
        }else {
            referenciaBase.addListenerForSingleValueEvent(new ValueEventListener() {
                Usuario usuarioGuardado;
                String id;
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot datos: dataSnapshot.getChildren()) {
                        Usuario usuario = datos.getValue(Usuario.class);
                        mensajeLog("USUARIO-Objeto", usuario.toString());
                        mensajeLog("USUARIO-usuarioCampo ", usuario.getUsuario());

                        if (user.equals(usuario.getUsuario().trim()) || user.equals(usuario.getCorreo().trim())){
                            usuarioGuardado = usuario;
                            id = datos.getKey();
                            mensajeLog("Uid: ", id);
                            break;
                        } else {
                            mensajeLog("No encontrado", "Usuario no Coincide!");
                        }
                    }

                    if (usuarioGuardado != null){

                        autenticacion.signInWithEmailAndPassword(usuarioGuardado.getCorreo(), txtLoginContrasena.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()){
                                    mensajeLog("EXITO: ", "Logueado Correctamente!!!");

                                    Intent i = new Intent(LoginPrincipal.this, BuscarParadaInvitado.class);
                                    i.putExtra("objeto", usuarioGuardado);
                                    startActivity(i);

                                    MainActivity.actividadMain.finish();

                                    finish();

                                }
                                else {
                                    mensajeToast("Problema de inicio de Sesión.\nRevise los datos e intente nuevamente");
                                    mensajeLog("PROBLEMA: ", "No se pudo Loguear");
                                }
                            }
                        });
                    }else {
                        mensajeLog("UsuarioGuardado es","NULL");

                        mensajeToast("Usuario no encontrado");
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                    mensajeLog("Problema", databaseError.getDetails());
                }
            });
        }


    }


    @Override
    protected void onStart() {
        super.onStart();
//        if (autenticacion.getCurrentUser() != null){
//
//            Intent i = new Intent(LoginPrincipal.this, BuscarParadaInvitado.class);
//            i.putExtra("objeto", autenticacion.getCurrentUser());
//
//            startActivity(i);
//            finish();
//        }

    }

    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }
}

