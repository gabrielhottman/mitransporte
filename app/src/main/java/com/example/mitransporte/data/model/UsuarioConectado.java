package com.example.mitransporte.data.model;

/**
 * Clase de datos que captura la información del usuario para los usuarios registrados que se recuperaron de LoginRepository
 */
public class UsuarioConectado {

    private String userId;
    private String displayName;

    public UsuarioConectado(String userId, String displayName) {
        this.userId = userId;
        this.displayName = displayName;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }
}
