package com.example.mitransporte.data;

import com.example.mitransporte.data.model.UsuarioConectado;

/**
 * Clase que solicita autenticación e información del usuario del origen de datos remoto y
 * mantiene una memoria caché en memoria del estado de inicio de sesión y la información de credenciales de usuario.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;

    private FuenteDeDatosDeInicioDeSesión dataSource;

    // Si las credenciales del usuario se almacenarán en caché en el almacenamiento local, se recomienda que se cifre
    // @see https://developer.android.com/training/articles/keystore
    private UsuarioConectado user = null;

    // private constructor : único access
    private LoginRepository(FuenteDeDatosDeInicioDeSesión dataSource) {
        this.dataSource = dataSource;
    }

    public static LoginRepository getInstance(FuenteDeDatosDeInicioDeSesión dataSource) {
        if (instance == null) {
            instance = new LoginRepository(dataSource);
        }
        return instance;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public void logout() {
        user = null;
        dataSource.cerrarSesion();
    }

    private void setLoggedInUser(UsuarioConectado user) {
        this.user = user;
        // Si las credenciales del usuario se almacenarán en caché en el almacenamiento local, se recomienda que se cifre
        // @see https://developer.android.com/training/articles/keystore
    }

    public Result<UsuarioConectado> login(String username, String password) {
        // manejar inicio de sesión
        Result<UsuarioConectado> result = dataSource.login(username, password);
        if (result instanceof Result.Success) {
            setLoggedInUser(((Result.Success<UsuarioConectado>) result).getData());
        }
        return result;
    }
}
