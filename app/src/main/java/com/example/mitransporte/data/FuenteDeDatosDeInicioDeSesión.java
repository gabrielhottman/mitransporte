package com.example.mitransporte.data;

import android.content.Intent;

import com.example.mitransporte.data.model.UsuarioConectado;

import java.io.IOException;

/**
    Clase que maneja la autenticación con credenciales / login y recupera información del usuario.
 **/
public class FuenteDeDatosDeInicioDeSesión {

    public Result<UsuarioConectado> login(String username, String password) {

        try {
            // TODO: manejar la autenticación de loginInUser

            UsuarioConectado fakeUser = new UsuarioConectado(java.util.UUID.randomUUID().toString(),"Usuario Fake");

            return new Result.Success<>(fakeUser);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error al iniciar sesión", e));
        }
    }

    public void cerrarSesion() {
        // TODO: revocar autenticación

    }
}
