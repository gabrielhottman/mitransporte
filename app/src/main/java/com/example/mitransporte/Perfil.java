package com.example.mitransporte;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.mitransporte.model.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class Perfil extends AppCompatActivity {

    EditText txtNombres, txtApellidos, txtCedula, txtFechaNacimiento, txtDireccion, txtSector;
    TextView tvLineaPerfil;
    RadioButton rbMasculino, rbFemenino;
    Button btnGuardarPerfil, btnClearCache;
    ImageView imagenPerfil;
    ProgressDialog progressDialog;

    FirebaseAuth autenticacion;
    DatabaseReference referenciaUsuarios;
    StorageReference referenciaStorage;


    private boolean hayCambios;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        getSupportActionBar().hide();
        hayCambios = false;


        autenticacion = FirebaseAuth.getInstance();
        referenciaStorage = FirebaseStorage.getInstance().getReference("FotosDeUsuarios");
        referenciaUsuarios = FirebaseDatabase.getInstance().getReference("Usuarios");

        txtNombres = findViewById(R.id.txtNombresPerfil);
        txtApellidos = findViewById(R.id.txtApellidosPerfil);
        txtCedula = findViewById(R.id.txtCedulaPerfil);
        txtFechaNacimiento = findViewById(R.id.txtFechaNacimientoPerfil);
        rbMasculino = findViewById(R.id.rbMasculino);
        rbFemenino = findViewById(R.id.rbFemenino);
        txtDireccion = findViewById(R.id.txtDireccionPerfil);
        txtSector = findViewById(R.id.txtSectorPerfil);
        tvLineaPerfil = findViewById(R.id.tvLineaPerfil);

        progressDialog = new ProgressDialog(this);

        btnGuardarPerfil = findViewById(R.id.btnGuardarPerfil);

        btnClearCache = findViewById(R.id.btnClearCache);

        imagenPerfil = findViewById(R.id.imagenFotoDePerfil);

        imagenPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                establecerImagenPerfil();

            }
        });

        btnGuardarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarPerfilFirebase();
                bloquearCamposPerfil();
            }
        });

        btnClearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                desbloquearCamposPerfil();
                //Glide.get(Perfil.this).clearDiskCache();
            }
        });


        loadInfoUsuarioFirebase();


        cargarPreferencias();
    }

    public void loadInfoUsuarioFirebase(){

        referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    mensajeLog("Si existe: ", "DATASNAPSHOT");
                    Usuario usuario = dataSnapshot.getValue(Usuario.class);

                    txtNombres.setText(usuario.getNombres());
                    txtApellidos.setText(usuario.getApellidos());
                    txtCedula.setText(usuario.getCedula());
                    txtFechaNacimiento.setText(usuario.getFechaNacimiento());

                    if (usuario.getGenero()!=null){
                        if (usuario.getGenero().equals("Masculino")){
                            rbMasculino.setChecked(true);
                        }
                        if (usuario.getGenero().equals("Femenino")){
                            rbFemenino.setChecked(true);
                        }
                    }

                    txtDireccion.setText(usuario.getDireccion());
                    txtSector.setText(usuario.getSectorDomicilio());

                    tvLineaPerfil.setText(usuario.getLineaBus());
                }else {
                    mensajeLog("datasnapshot", "NO EXISTE");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("Error", databaseError.toException());
            }
        });
    }


    public void guardarPerfilFirebase(){

        if (verificaCamposObligatorios()){

            String nombres = txtNombres.getText().toString().trim();
            String apellidos = txtApellidos.getText().toString().trim();
            String cedula = txtCedula.getText().toString().trim();
            String genero = "";
            if (rbMasculino.isChecked()){
                genero = rbMasculino.getText().toString();
            }else if (rbFemenino.isChecked()){
                genero = rbFemenino.getText().toString();
            }

            String fechaNacimiento = txtFechaNacimiento.getText().toString();
            String direccion = txtDireccion.getText().toString().trim();
            String sector = txtSector.getText().toString().trim();

            final Usuario usuario = new Usuario(nombres, apellidos,cedula,fechaNacimiento, genero, sector, direccion);



            referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).child("nombres").setValue(usuario.getNombres()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        UserProfileChangeRequest actualizaDeisplayName = new UserProfileChangeRequest.Builder()
                                .setDisplayName(usuario.getNombres()).build();
                        autenticacion.getCurrentUser().updateProfile(actualizaDeisplayName).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    mensajeLog("updateProfile", "displayName actualizado correctamente");
                                        hayCambios = true;
                                        SharedPreferences preferences = getSharedPreferences("displayName", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putBoolean("hayCambios", hayCambios);
                                        editor.commit();
                                }else{
                                    mensajeLog("updateProfile", "displayName No Pudo Actualizar");
                                }
                            }
                        });

                    }
                }
            });
            referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).child("apellidos").setValue(usuario.getApellidos());
            referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).child("cedula").setValue(usuario.getCedula());
            referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).child("fechaNacimiento").setValue(usuario.getFechaNacimiento());
            referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).child("genero").setValue(usuario.getGenero());
            referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).child("direccion").setValue(usuario.getDireccion());
            referenciaUsuarios.child(autenticacion.getCurrentUser().getUid()).child("sectorDomicilio").setValue(usuario.getSectorDomicilio()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    mensajeToast("Datos guardados Correctamente.");
                }
            });
        }

    }

    public boolean verificaCamposObligatorios(){
        if (txtNombres.getText().toString().trim().isEmpty()){
            mensajeToast("Debe escribir su nombre");
            return false;
        }else if (txtApellidos.getText().toString().trim().isEmpty()){
            mensajeToast("Debe escribir su apellido");
            return false;
        }else if (txtCedula.getText().toString().trim().isEmpty()){
            mensajeToast("Escriba su Cédula");
            return false;
        }else if (!rbMasculino.isChecked() && !rbFemenino.isChecked()){
            mensajeToast("Debe Seleccionar su Género");
            return false;
        }


        if (txtDireccion.getText().toString().trim().isEmpty()){
            txtDireccion.setText("S/D");
        }
        if (txtSector.getText().toString().trim().isEmpty()){
            txtSector.setText("S/D");
        }
        if (txtFechaNacimiento.getText().toString().trim().isEmpty())
            txtFechaNacimiento.setText("S/D");


        return true;


    }

    private void cargarPreferencias() {
        SharedPreferences preferences = getSharedPreferences("imagen", Context.MODE_PRIVATE);

        String uri = preferences.getString("img", "NoHayImg");

        mensajeLog("uri ", uri);
        mensajeLog("uri ", "-----------------");

        if (!uri.equals("NoHayImg")){

            Glide.with(Perfil.this).load(uri).fitCenter().centerCrop().into(imagenPerfil);
        }


    }





    private void establecerImagenPerfil() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        mensajeLog("Intent  ", intent.toString());
        startActivityForResult(intent.createChooser(intent, "Seleccione la Aplicación"),10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){

            final Uri uri = data.getData();

            progressDialog.setTitle("Cargando");
            progressDialog.setMessage("Espere por favor...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            //imagenPerfil.setImageURI(uri);

            if (!obtenerIdUsuario().isEmpty()){

                StorageReference filePath = referenciaStorage.child(obtenerIdUsuario()).child(uri.getLastPathSegment());

                mensajeLog("Uri: ", uri.getLastPathSegment());
                filePath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        progressDialog.dismiss();
                        mensajeToast("Su Foto fue Guardada exitosamente!");


                        referenciaStorage.child(obtenerIdUsuario()).child(uri.getLastPathSegment()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {

                                Glide.with(Perfil.this).load(uri).fitCenter().centerCrop().into(imagenPerfil);

                                mensajeLog("Guardado",uri.toString());


                                SharedPreferences preferences = getSharedPreferences("imagen", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("img", uri.toString());
                                editor.commit();
                            }
                        });




                    }
                });

            }


        }

    }



    //verifica y obtiene el Usuario conectado
    public String obtenerIdUsuario(){
        String id ="";

        if (autenticacion.getCurrentUser() != null){
            id = autenticacion.getCurrentUser().getUid();
        }else{
            mensajeToast("No hay Usuario Conectado");
        }

        return id;
    }





    //Desbloquea los EditTexts para poder editarlos
    private void desbloquearCamposPerfil(){
        txtNombres.setEnabled(true);
        txtApellidos.setEnabled(true);
        txtCedula.setEnabled(true);
        txtFechaNacimiento.setEnabled(true);
        rbMasculino.setEnabled(true);
        rbFemenino.setEnabled(true);
        txtDireccion.setEnabled(true);
        txtSector.setEnabled(true);
    }
    //Bloquea los EditTexts
    private void bloquearCamposPerfil(){
        txtNombres.setEnabled(false);
        txtApellidos.setEnabled(false);
        txtCedula.setEnabled(false);
        txtFechaNacimiento.setEnabled(false);
        rbMasculino.setEnabled(false);
        rbFemenino.setEnabled(false);
        txtDireccion.setEnabled(false);
        txtSector.setEnabled(false);
    }













    public void mensajeToast(String msj){
        Toast.makeText(this, msj, Toast.LENGTH_SHORT).show();
    }
    public void mensajeLog(String tag, String msj){
        Log.e(tag, msj);
    }
}
